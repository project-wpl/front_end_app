import React, {Component} from 'react';
import DB from '../resources/db.json';
import BookCard from '../components/bookcard';
class BookList extends Component {

    constructor(props) {
        super(props)
        this.state = {
            booklist: DB
        }
    }

    render(){
        return(
            <div>
                {this.state.booklist.map(book => (
                    <BookCard key={book.id} book={book}/>
                ))}
            </div>
           
        )
    }
}

export default BookList