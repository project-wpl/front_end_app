import React from 'react'
import uploadImage from '../resources/images/upload.png'
import { Button } from 'antd';

export default class UploadBook extends React.Component {
    render() {
        return(
            <div style={{textAlign: "center", backgroundColor:"white", padding:'10px', border: '1px solid gray'}}>
                <img alt="upload  icon" src={uploadImage} style={{padding: '10px'}} />
                <h1>Want to contribute !</h1>
                <Button type="primary" shape="round" icon="upload" size='large'>
                    Upload A Book
                </Button>
                
            </div>
        )
    }
}