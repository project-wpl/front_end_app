import React from 'react';
import { Card } from 'antd';
import 'antd/dist/antd.css'
import { Row, Col } from 'antd';


class BookCard extends React.Component {
    componentWillMount() {
        console.log(this.props.book)
    }
    render() {
        return(
            <div>
                <Card  style={{ margin: 30}}>
                    <div >
                    <h1>{this.props.book.title}</h1>
                        <Row>
                            <Col span={6} push={18}>
                                <div style={{float:'right'}}>
                                <img alt={this.props.book.title} src={this.props.book.imageurl} style={{padding: '10px'}} />
                                </div>    
                            </Col>
                            <Col span={18} pull={6}>
                                <p>Author: {this.props.book.author}</p>
                                <p>Description: {this.props.book.description}</p>
                            </Col>
                        </Row>
                        {/* <div style ={{float:'left', width: '30%'}}>
                            <img alt={this.props.book.title} src={this.props.book.imageurl} />
                        </div>
                        <div style ={{float:'right', marginLeft: '10px', width: '70%', position:'relative'}}>
                            <p>Author: {this.props.book.author}</p>
                            <p>Description: {this.props.book.description}</p>
                        </div> */}
                    </div>
                    
                </Card>
            </div>
        )
    }
}

export default BookCard;