import React, {Component} from 'react'
import 'antd/dist/antd.css'
import {
    Menu
} from 'antd'

class Navigationbar extends Component {

    render (){
        return(
            <div>
                <div className="logo" style={{float:'left'}} >
                    <h1 style= {{color: "white"}}>World Public Library</h1>
                </div>
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={['1']}
                        style={{ lineHeight: '64px', float:'right' }}
                    >
                    <Menu.Item key="1">Home</Menu.Item>
                    </Menu>
            </div>
        )
    }

}

export default Navigationbar