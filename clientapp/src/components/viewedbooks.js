import React from 'react'

const books = [
    "ULYSSES by James Joyce.",
    "THE GREAT GATSBY by F. Scott Fitzgerald.",
    "A PORTRAIT OF THE ARTIST AS A YOUNG MAN by James Joyce.",
    "LOLITA by Vladimir Nabokov.",
    "BRAVE NEW WORLD by Aldous Huxley.",
    "THE SOUND AND THE FURY by William Faulkner.",
    "CATCH-22 by Joseph Heller.",
    "DARKNESS AT NOON by Arthur Koestler."
]

export default class ViewedBooks extends React.Component {
    
    render(){
        return(
            <div style={{textAlign: "center", backgroundColor:"white", padding:'10px', border: '1px solid gray'}}>
                <h1>
                    Most Viewed Books
                </h1>
                <br/>
                {books.map(book => (
                    <p>{book}</p>
                ))}
            </div>
            
        )
    }
}