import React, {Component} from 'react';
import Navigationbar from '../components/header';

import { Layout, Col, Row, Input } from 'antd';
import 'antd/dist/antd.css'

import BookList from '../components/booklist';
import UploadBook from '../components/uploadbook';
import ViewedBooks from '../components/viewedbooks';

const { Header, Content, Footer } = Layout;
const { Search } = Input;

class Home extends Component {
    render() {
        return(
            <Layout>
                <Header style={{ top: "0" }}>
                    <Navigationbar/>
                </Header>
                <Content>
                    {/* <div style={{ background: "white", height: "calc(100vh - 55px)" }}> */}
                        <br/>
                        <div style={{textAlign: "center"}}>
                            <Search 
                                placeholder="Enter the name of the book"
                                enterButton="Search"
                                size="large"
                                onSearch={value => console.log(value)}
                                style={{width:'50%'}}
                            />
                            <h1 style={{marginTop: '10px'}}>Welcome to World Public Library</h1>
                        </div>
                        <br/>
                        <Row>
                            <Col span={18} style={{padding: '10px'}}>
                                <BookList/>
                            </Col>
                            <Col span={6} style={{ padding: '10px'}}>
                                <UploadBook/>
                                <br/>
                                <ViewedBooks/>
                            </Col>
                        </Row>
                    {/* </div> */}
                </Content>
                <br/>
                <Footer style={{ textAlign: 'center', bottom:0}}><b>World Public Library</b> © 2019</Footer>
            </Layout>
        )
    }
}

export default Home;